My T480 had the [classic symptoms of a Thunderbolt port failure in this series of machines](https://support.lenovo.com/us/en/solutions/ht508988-critical-intel-thunderbolt-software-and-firmware-updates-thinkpad).  Low power charging only, even after all the firmware updates had been applied.  This happened very shortly after purchasing the machine (within weeks).  After some research, it seems that it's not that the EEPROM is damaged, but "full" from the writes that the firmware was doing.  So, like a trashcan, you need to empty it, and that's what we're gonna do today.

The basic steps are:
1. Backup the contents of what's on EEPROM
2. Erase the EEPROM
3. Zero out the EEPROM
4. Flash our new bin to the EEPROM
5. Profit!

I had a CH341aPro and SOIC8 clip ready to do this to the MXIC MX251800SE EEPROM chip located next to the Thunderbolt ports on the machine, however was having issues using Flashrom with the CH341aPro.  So instead, I used a RasPi 3B I had to do the job.  There is already a [great article on setting a RasPi up for use as an SPI interface](https://tomvanveen.eu/flashing-bios-chip-raspberry-pi/).  Follow hooking it up with the [pinout for the MX25L8006E image](https://gitlab.com/MobileAZN/lenovo-t480-thunderbolt-firmware-fixes/-/blob/main/MX25L8006E.jpg).

Once you get the RasPi all set up, we need to prep the Thunderbolt EEPROM files (bin).  The [Thunderbolt firmware chip](https://gitlab.com/MobileAZN/lenovo-t480-thunderbolt-firmware-fixes/-/blob/main/eeprom.jpeg) is a 1MB (1048576 byte) chip.  We need a null.bin to write zeros to the EEPROM and the actual Thunderbolt firmware padded with extra zeros to fill the EEPROM with the size that is expected.  What you get from Lenovo is smaller and their program then fills it afterwards.  We are gonna do it all beforehand.

To make a null.bin file- just a file we're gonna flash full of 0s- we run this:
```
dd if=/dev/zero of=null.bin bs=1M count=1
```

Now let's make the EEPROM flash file.  We need the TBT.bin from Lenovo's installer.  I extracted mine on a Windows machine.  I just ran the installer, but chose to extract only.  Then I went to the location the installer dumped them (C:\\win\\drivers\\thunderbolt\\).  Get that file back to your linux side/copy it to the RasPi and let's pad it:
```
dd if=/dev/null of=TBT.bin bs=1 seek=1048576
```
This takes the TBT.bin file we grabbed from Lenovo and allocates the file to be the correct size.

Now that we have the files prepped, let's do this.
Boot the T480 into BIOS and let's turn OFF Thunderbolt Assist mode.  We really don't need this anyway with newer OSes.  Let's also disable the internal battery as we don't want anything to happen.  I only did this the first time, I didn't do it subsequent reboots/flashes.  When you disable the battery, the machine will shut down on it's own.

Hook your machine up to the SOIC8 clip and RasPi and back that thing up:
```
flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=512 -r tb_backup.bin
```
This READS (-r) the chip and makes a backup named tb_backup.bin. 

Now that we have a backup, we can erase the EEPROM:
```
flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=512 -E
```

Now let's zero it out.  We'll take that null.bin file we made earlier and flash it (WRITE it with -w):
```
flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=512 -w null.bin
```

At this point, I disconnected the clip and rebooted the T480 (plug in power, turn it on) as I'd read in a few other articles that a reboot here is necessary.  I'm not sure that it is, but I did it and it worked for me.  

Now that's it's all cleared out, let's get the good stuff on there.  Power back down, hook the SOIC8 clip back up and run:
```
flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=512 -w TBT.bin
```

Once this is done, unhook the clip, plug in power and start your machine up.  It should see Thunderbolt and you should be back to a good working state.

Hope this helps!  Many thanks to the various articles and forums I've read over the past few weeks to gather all of this.  These steps should be fairly transferable to any other Lenovo machine on that list, you'll just need to use the correct base TBT.bin file and adjust if the size of their EEPROM is different (most with this issue are the same 1048676 bytes). Also, this is what you’d do if you solder in a new EEPROM.  Let me know of any corrections, I'll edit this to keep it up to date.

For convenience, I've hosted the [null.bin](https://gitlab.com/MobileAZN/lenovo-t480-thunderbolt-firmware-fixes/-/blob/main/null.bin) and my expanded [TBT_padded.bin](https://gitlab.com/MobileAZN/lenovo-t480-thunderbolt-firmware-fixes/-/blob/main/TBT_padded.bin) on GitLab.


## So, for the TL;DR
1. [Setup RasPi](https://tomvanveen.eu/flashing-bios-chip-raspberry-pi/) with SOIC8 clip
2. Boot into BIOS and turn off Thunderbolt Assist and Internal Battery
3. Backup Thunderbolt EEPROM
```
flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=512 -r tb_backup.bin
```
4. Erase EEPROM
```
flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=512 -E
```
5. Write [null.bin](https://gitlab.com/MobileAZN/lenovo-t480-thunderbolt-firmware-fixes/-/blob/main/null.bin) to EEPROM
```
flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=512 -w null.bin
```
6. Unclip and reboot.

7. Shutdown and reclip

8. Write [TBT_padded.bin](https://gitlab.com/MobileAZN/lenovo-t480-thunderbolt-firmware-fixes/-/blob/main/TBT_padded.bin) to EEPROM
```
flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=512 -w TBT_padded.bin
```
9. Unclip and enjoy working Thunderbolt
